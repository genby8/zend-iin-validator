<?php

namespace Genby\ZendIinValidator;

use Zend\Filter\Digits;
use Zend\Validator\AbstractValidator;

class Iin extends AbstractValidator
{
	const NOT_DIGITS = 'notDigits';
	const STRING_EMPTY = 'stringEmpty';
	const INVALID = 'digitsInvalid';
	const LENGTH = 'length';

	/**
	 * Digits filter used for validation
	 *
	 * @var Digits
	 */
	protected static $filter = null;

	/**
	 * Validation failure message template definitions
	 *
	 * @var array
	 */
	protected $messageTemplates = [
		self::NOT_DIGITS => "The input must contain only digits",
		self::STRING_EMPTY => "The input is an empty string",
		self::INVALID => "Invalid type given. String, integer expected",
		self::LENGTH => "The input is not equal than %length% characters long",
	];

	/**
	 * @var array
	 */
	protected $options = [
		'length' => 12
	];

	/**
	 * @var array
	 */
	protected $messageVariables = [
		'length' => ['options' => 'length']
	];


	/**
	 * Returns the length option
	 *
	 * @return int
	 */
	private function getLength()
	{
		return $this->options['length'];
	}

	/**
	 * Returns true if and only if $value only contains iin
	 *
	 * @param  string $value
	 * @return bool
	 */
	public function isValid($value)
	{
		if (!is_string($value) && !is_int($value)) {
			$this->error(self::NOT_DIGITS);
			return false;
		}

		$this->setValue((string)$value);

		if ('' === $this->getValue()) {
			$this->error(self::STRING_EMPTY);
			return false;
		}

		if (null === static::$filter) {
			static::$filter = new Digits();
		}

		if ($this->getValue() !== static::$filter->filter($this->getValue())) {
			$this->error(self::NOT_DIGITS);
			return false;
		}
		if (mb_strlen($this->getValue()) !== $this->getLength()) {
			$this->error(self::LENGTH);
			return false;
		}

		return true;
	}
}